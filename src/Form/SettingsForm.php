<?php

namespace Drupal\didsomeonecloneme\Form;

use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\Core\Link;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Defines a form to configure Did Someone Clone Me settings.
 */
class SettingsForm extends ConfigFormBase {

  /**
   * The language manager.
   *
   * @var \Drupal\Core\Language\LanguageManagerInterface
   */
  protected $languageManager;

  /**
   * The module handler.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * Constructor.
   *
   * @param \Drupal\Core\Language\LanguageManagerInterface $language_manager
   *   The language manager.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler.
   */
  public function __construct(LanguageManagerInterface $language_manager, ModuleHandlerInterface $module_handler) {
    $this->languageManager = $language_manager;
    $this->moduleHandler = $module_handler;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('language_manager'),
      $container->get('module_handler')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'didsomeonecloneme_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['didsomeonecloneme.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    // Get immutable config.
    $config = $this->configFactory()->get('didsomeonecloneme.settings');

    $form['title'] = [
      '#type' => 'item',
      '#title' => $this->t('<h2>Did someone clone me?</h2>')
    ];
    
    $dscm_url = Url::fromUri('https://didsomeoneclone.me/')->toString();

    $form['description'] = [
      '#type' => 'item',
      '#title' => $this->t("<p>Please fill you personal link below.<br>Don't have a link yet? Request a new link at <a href=':url' target='_blank'>https://didsomeoneclone.me/</a>.</p>", [':url' => $dscm_url]),
      '#prefix' => '<div>',
      '#suffix' => '</div>',
    ];

    $form['dscm_url'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Your link'),
      '#default_value' => $config->get('dscm_url'),
      '#description' => $this->t("", [':url' => $dscm_url]),
      '#required' => TRUE,
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    // check if dscm url is added & is correct
    if (is_null($form_state->getValue('dscm_url')) || !(preg_match("/^(http|https):\/\//i", $form_state->getValue('dscm_url'), $match))) {
      $form_state->setErrorByName('dscm_url', $this->t('You must enter a valid URL.'));
    }
    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $values = $form_state->getValues();

    // Save the configuration changes.
    $dscm_config = $this->config('didsomeonecloneme.settings');
    $dscm_config->set('dscm_url', $values['dscm_url']);

    $dscm_config->save();

    parent::submitForm($form, $form_state);
  }
}
